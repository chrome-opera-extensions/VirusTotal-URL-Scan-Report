;((global, CreateHTMLElement) => {
     global.vtContainer = (txt, author) => {
          if(!document.querySelector("#vt-overlay")) {
               global.virustotal = {
                    author: `${author}`,
                    scanUrl: "https://www.virustotal.com/vtapi/v2/url/scan",
                    reportUrl: "https://www.virustotal.com/vtapi/v2/url/report",
                    get(url, data) {
                         return new Promise((resolve, reject) => {
                              var arr = [];
                              for(var e in data) {
                                   if(data.hasOwnProperty(e)) {
                                        arr.push(e + '=' + encodeURIComponent(data[e]));
                                   }
                              }
                              data = arr.join("&");
                              var xhr = new XMLHttpRequest();
                              xhr.open(data ? "POST" : "GET", url, true);
                              xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                              xhr.onreadystatechange = function() {
                                   if(this.readyState === 4 && this.status === 200) {
                                        resolve(xhr.responseText);
                                   } else if(this.readyState === 4 && this.status === 204) {
                                        var responseheaders = xhr.getAllResponseHeaders();
                                        responseheaders = xhr.getResponseHeader("x-api-message");
                                        reject(responseheaders);
                                   }
                              };
                              xhr.onerror = function() {
                                   reject(this);
                              };
                              xhr.send(data);
                         });
                    },
                    sleep(time, fn) {
                         return new Promise((resolve) => setTimeout(resolve, time));
                    },
                    sleep2(time, fn) {
                         return new Promise((resolve) => {
                              var cycle = setInterval(() => {
                                   fn();
                              }, time);
                         });
                    }
               };
               if(!document.querySelector("#vt-overlay-css")) {
                    var cssText = `#vt-overlay{height: 100% !important;}`;
                    cssText += `#vt-exit,#vt-inner-container{position: absolute !important;}`;
                    cssText += `#vt-overlay {position: fixed !important;display: none;width: 100% !important;top: 0 !important;left: 0 !important;right: 0 !important;bottom: 0 !important;background-color: rgba(0, 0, 0, .5) !important;z-index: 200000 !important;}`;
                    cssText += `#vt-inner-container {top: 50% !important;left: 50% !important;height: 90% !important;width: 80% !important;z-index: 1 !important;font-size: 14px !important;font-family: Arial !important;color: #000 !important;background-color: #fff !important;transform: translate(-50%, -50%) !important;-ms-transform: translate(-50%, -50%) !important;}`;
                    cssText += `#vt-exit {top: 5px !important;right: 5px !important;z-index: 20000000 !important;font-size: 25px !important;font-weight: 700 !important;color: #fff !important;cursor: pointer !important;}`;
                    cssText += `#vt-text-container {padding-top: 20px !important;width: 80% !important;height: 90% !important;margin: auto !important;}`;
                    cssText += `#vt-text-container h3 {border-bottom: 1px solid black !important; width: 100% !important; margin-bottom: 5px !important; font: 400 1.17em Arial !important;color: black !important;}`;
                    cssText += `#vt-textbox {width: 100% !important;display: grid !important;margin-bottom: 5px !important;border: 1px solid black !important;height: 19px !important;padding: 0px !important;font: 400 13.3333px Arial !important;}`;
                    cssText += `#vt-textbox:focus {box-shadow: inset 0 1px 3px rgba(0,0,0,0.06), 0 0 6px 0px #106fb2 !important;outline: none !important;}`;
                    cssText += `#vt-submit-btn {float: right !important;margin-bottom: 25px !important;margin-top: 0px !important;margin-left: 0px !important;margin-right: 0px !important;font-weight: normal !important;background: none !important;border: 1px solid transparent !important;border-radius: 3px !important;`;
                    cssText += `box-shadow: none !important;color: #FFF !important;background-color: #0095ff !important;border: 1px solid #07c !important;box-shadow: inset 0 1px 0 #66bfff !important;box-sizing: border-box !important;font-size: 100% !important;position: relative !important;`;
                    cssText += `padding: .6em !important;outline: none !important;font-family: inherit !important;line-height: 1.15384615 !important;text-decoration: none !important;cursor: pointer !important;padding-top: 8px !important;padding-bottom: 8px !important;vertical-align: baseline;white-space: nowrap !important;list-style: none !important;}`;
                    cssText += `#vt-submit-btn:hover {background-color: #007ed9 !important;}`;
                    cssText += `#vt-textbox-label {margin-bottom: 5px !important;}`;
                    cssText += `#vt-output {width: 100% !important; height: calc(95% - 140px) !important;overflow-x: hidden !important;}`;
                    cssText += `#vt-output-table {width:100% !important;border-collapse: collapse !important;margin: 0px !important;padding: 0px !important; color: black;font-size: 14px !important;font-family: Arial !important;}`;
                    cssText += `#vt-output-table td:nth-child(2),#vt-output-table td:nth-child(3) {text-align: center !important;}`;
                    cssText += `#vt-output-table td:nth-child(1),#vt-output-table td:nth-child(2){width: 33% !important;}`;
                    cssText += `#vt-output-table thead {background-color: lightsteelblue !important;}`;
                    cssText += `#vt-output-table th,#vt-output-table td {border: 1px solid black !important;}`;
                    cssText += `#vt-output-table th {text-align: center !important; font-weight: bold !important;height: 20px !important;padding: 0px !important;line-height: 20px !important;}`;
                    cssText += `#vt-output-table td {height: 20px !important;padding: 0px !important;line-height: 20px !important;}`;
                    cssText += `#vt-error {width: 90% !important; margin: auto !important;}`;
                    cssText += `#vt-output-table tbody tr:nth-child(odd) {background-color: #f1f0f0 !important;}`;
                    cssText += `#vt-output-table tbody tr:hover {background-color: #d7d7ff !important;}`;
                    cssText += `#vt-output-table caption {border-top: 1px solid black !important;border-left:1px solid black !important;border-right:1px solid black !important;background-color: black !important; color: white !important;height: 20px !important;padding: 0px !important;line-height: 20px !important;text-align: center !important;}`;
                    document.head.appendChild(CreateHTMLElement(`<style id="vt-overlay-css" type="text/css">${cssText}</style>`));
               }

               var vtInnerContainer = CreateHTMLElement(`<div id="vt-inner-container" name="text"></div>`);
               var vtTextContainer = CreateHTMLElement(`<div id="vt-text-container"><h3>Virustotal URL Scan</h3></div>`);
               vtTextContainer.innerHTML += `<div id="vt-textbox-label">Enter url:</div><input id="vt-textbox" type="text" name="vt-textbox" value="${txt}">`;
               var vtSubmitBtn = CreateHTMLElement({
                    html: `<input id="vt-submit-btn" name="vt-submit-btn" type="submit" value="Submit">`,
                    fn: () => {
                         var resource = document.querySelector("#vt-textbox").value;
                         console.log(resource);
                         var lamp = atob("eWVraXBh").split("").reverse().join("");
                         var scanexport = {};
                         scanexport.url = resource;
                         scanexport[lamp] = virustotal.author
                         var reportexport = {};
                         reportexport.resource = resource;
                         reportexport[lamp] = virustotal.author;
                         virustotal.get(virustotal.scanUrl, scanexport).then((report) => {
                              function getReport() {
                                   virustotal.get(virustotal.reportUrl, reportexport).then((response) => {
                                        console.log(response);
                                        var GOOD_RESPONSE = false;
                                        try {
                                             JSON.parse(response).response_code;
                                             GOOD_RESPONSE = true;
                                        } catch (ex) {
                                             virustotal.sleep(3000).then(() => getReport());
                                             return;
                                        }
                                        if(GOOD_RESPONSE) {
                                             if(JSON.parse(response).response_code === 0) {
                                                  virustotal.sleep(3000).then(() => getReport());
                                             } else {
                                                  response = JSON.parse(response);
                                                  var table = CreateHTMLElement(`<table id="vt-output-table"><caption>${txt}</caption><thead><tr><th>Site</th><th>Detected</th><th>Result</th></tr></thead></table>`);
                                                  var tbody = CreateHTMLElement('<tbody></tbody>');
                                                  var scans = response.scans;
                                                  for(let scan in scans) {
                                                       if(scans.hasOwnProperty(scan)) {
                                                            var rcolor = "";
                                                            if(scans[scan].result.toUpperCase() === "CLEAN SITE") {
                                                                 rcolor = "green";
                                                            } else if(scans[scan].result.toUpperCase() === "UNRATED SITE") {
                                                                 rcolor = "orange";
                                                            } else {
                                                                 rcolor = "red";
                                                            }
                                                            var dcolor = (scans[scan].detected) ? "red" : "black";
                                                            var tr = CreateHTMLElement(`<tr><td>${scan}</td><td style="color:${dcolor}">${scans[scan].detected.toString().toUpperCase()}</td><td style="color:${rcolor};">${scans[scan].result.toUpperCase()}</td></tr>`);
                                                            tbody.appendChild(tr);
                                                       }
                                                  }
                                                  table.appendChild(tbody);
                                                  var vtOutput = document.querySelector("#vt-output");
                                                  if(document.querySelector("#vt-output-table")) {
                                                       document.querySelector("#vt-output-table").replaceWith(table);
                                                  } else {
                                                       vtOutput.appendChild(table);
                                                  }
                                                  vtOutput.style.overflowY = "scroll";
                                             }
                                        }
                                   }).catch((ex) => {
                                        var tempTable = CreateHTMLElement(`<table id="vt-output-table"><caption></caption><thead><tr><th>E R R O R</th></tr></thead><tbody><tr><td id="vt-error"><div id="vt-error">${ex}</div></td></tr></tbody></table>`);
                                        var vtOutput = document.querySelector("#vt-output");
                                        if(document.querySelector("#vt-output-table")) {
                                             document.querySelector("#vt-output-table").replaceWith(tempTable);
                                        } else {
                                             vtOutput.appendChild(tempTable);
                                        }
                                   });
                              }
                              getReport();
                         }).catch((ex) => {
                              var tempTable = CreateHTMLElement(`<table id="vt-output-table"><caption></caption><thead><tr><th>E R R O R</th></tr></thead><tbody><tr><td><div id="vt-error">${ex}</div></td></tr></tbody></table>`);
                              var vtOutput = document.querySelector("#vt-output");
                              if(document.querySelector("#vt-output-table")) {
                                   document.querySelector("#vt-output-table").replaceWith(tempTable);
                              } else {
                                   vtOutput.appendChild(tempTable);
                              }
                         });
                    }
               });
               vtTextContainer.appendChild(vtSubmitBtn);
               var vtOutput = CreateHTMLElement(`<div id="vt-output"></div>`);
               var tempTable = CreateHTMLElement(`<table id="vt-output-table"><caption>Please Wait...</caption><thead><tr><th>Site</th><th>Detected</th><th>Result</th></tr></thead></table>`);
               vtOutput.appendChild(tempTable);
               vtTextContainer.appendChild(vtOutput);
               vtInnerContainer.appendChild(vtTextContainer);
               var vtExit = CreateHTMLElement({
                    html: `<span id="vt-exit">&#x2717;</span>`,
                    fn: () => {
                         document.getElementById("vt-overlay").remove();
                    }
               });
               var vtOverlay = CreateHTMLElement({
                    html: `<div id="vt-overlay" style="display: block;"></div>`,
                    fn: (evt) => {
                         if(evt.target.id === "vt-overlay") {
                              document.getElementById("vt-exit").click();
                         }
                    }
               });
               vtOverlay.appendChild(vtInnerContainer);
               vtOverlay.appendChild(vtExit);
               document.body.appendChild(vtOverlay);
               document.getElementById("vt-submit-btn").click();
               //global.virustotal = virustotal;
          }
     }
     //global.vtContainer = vtContainer;
})(
     window,
     function(items) {
          var html = (typeof(items) === "string") ? items : items.html;
          var fn = (typeof(items) === "object") ? items.fn : null;
          var template = document.createElement("template");
          template.innerHTML = html;
          template.content.firstChild.onclick = fn;
          return template.content.firstChild;

     }
);

//var CreateHTMLElement = function(items) {
// var html = (typeof(items) === "string") ? items : items.html;
// var html = (typeof(items) === "string") ? items : items.html;
// var fn = (typeof(items) === "object") ? items.fn : null;
// var template = (typeof unsafeWindow !== "undefined") ? unsafeWindow.document.createElement('template') : window.document.createElement('template');
// template.innerHTML = html;
// var elem = document.createElement(template.content.firstChild.tagName);
// template.content.firstChild.getAttributeNames().map((attr) => {
//      if(attr !== "style") {
//           elem[attr] = template.content.firstChild[attr];
//      } else {
//           elem[attr] = template.content.firstChild[attr].cssText;
//      }
// });
// elem.innerHTML = template.content.firstChild.innerHTML;
// elem.onclick = fn;
// return elem
//}