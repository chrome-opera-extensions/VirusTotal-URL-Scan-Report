function showNotification(data) {
     new Notification(data.title, {
          icon: 'images/favicon.png',
          body: data.body
     });
}

chrome.browserAction.onClicked.addListener(function() {
     showNotification({title: "Version", body: chrome.app.getDetails().version});
});

function vtScan(link) {
     chrome.tabs.query({
          "active": true,
          "lastFocusedWindow": true
     }, (tabs) => {
          chrome.tabs.executeScript(tabs[0].id, {
               file: "/scripts/vtscan.js"
          });
          setTimeout(function() {
               chrome.tabs.executeScript(tabs[0].id, {
                    code: 'vtContainer("' + link + '", "' + atob(chrome.app.getDetails().author + "==") + '");'
               });
          }, 200);
     });
}



function scanLink(evt) {
     var link = evt.linkUrl;
     //console.log(m);
     if(link) {
          vtScan(link);
     }
}

var VTCONTEXTMENU = null;
function createContextMenu() {
     var vtscanOptions = {
          title: "VirusTotal Scan Link",
          contexts: ["link"],
          id: "contextVTScanlink",
          onclick: scanLink
     }
     var vtscanUpdate = {
          title: "VirusTotal Scan Link",
          contexts: ["link"],
          onclick: scanLink
     }
     if(VTCONTEXTMENU !== undefined && VTCONTEXTMENU !== "undefined" && VTCONTEXTMENU !== null) {
          chrome.contextMenus.update(VTCONTEXTMENU, vtscanUpdate);
     } else {
          VTCONTEXTMENU = chrome.contextMenus.create(vtscanOptions);
     }
}

chrome.runtime.onInstalled.addListener((details) => {
     createContextMenu();
});

chrome.tabs.onCreated.addListener((e) => {
     if(chrome.extension.inIncognitoContext) {
          createContextMenu();
     }
     if(!chrome.extension.inIncognitoContext) {
          createContextMenu();
     }
});